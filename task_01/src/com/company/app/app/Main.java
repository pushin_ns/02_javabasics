package com.company.app.app;

import com.company.app.calculation.Calculations;
import com.company.app.domain.Course;
import com.company.app.domain.Curriculum;
import com.company.app.domain.Student;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.company.app.calculation.Calculations.*;

public class Main {

    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        Student student1;
        Student student2;
        Student student3;
        Student student4;
        LocalDate dateTime;
        Curriculum curr1;
        Curriculum curr2;
        Curriculum curr3;
        Curriculum curr4;
        List<Course> courses1 = new ArrayList<>();
        List<Course> courses2 = new ArrayList<>();
        List<Course> courses3 = new ArrayList<>();
        List<Course> courses4 = new ArrayList<>();
        int[] marks1 = {3};
        int[] marks2 = {3, 3};
        int[] marks3 = {4, 5, 5};
        int[] marks4 = {4, 5, 5};
        dateTime = LocalDate.now();

        curr1 = new Curriculum("J2EE Developer", courses1);
        curr2 = new Curriculum("Java Developer", courses2);
        curr3 = new Curriculum("Android Developer", courses3);
        curr4 = new Curriculum("Android Developer", courses4);

        courses1.add(new Course("Java Servlets", 8));
        courses1.add(new Course("Spring Framework", 16));

        courses2.add(new Course("JDBC", 8));
        courses2.add(new Course("JAX", 36));

        courses3.add(new Course("Java RX", 56));
        courses3.add(new Course("Concurrency", 26));

        courses4.add(new Course("Concurrency", 74));

        student1 = new Student("Ivan", curr1, dateTime, marks1);
        student2 = new Student("Fedor", curr2, dateTime, marks2);
        student3 = new Student("Gregory", curr3, dateTime, marks3);
        student4 = new Student("John", curr4, dateTime, marks4);

        students.add(student4);
        students.add(student3);
        students.add(student2);
        students.add(student1);
        System.out.println(students);

        //Getting the average of one student
        calculateProgress(student3);
        System.out.println();

        //Sorting by average mark
        System.out.println("Sorted by average mark");
        students.sort(Comparator.comparingDouble(Calculations::getAverage).reversed());
        students.forEach(Calculations::printAvgAndDays);

        //Sorting by days until the end
        System.out.println("\nSorted by remaining days");
        students.sort(Comparator.comparingDouble(Calculations::calculateDaysUntilEnd));
        students.forEach(Calculations::printAvgAndDays);

        //Calculating end date for Gregory
        System.out.println("\nGregory закончит обучение " + calculateEndDate(student3) + " - через " + calculateDaysUntilEnd(student3) + " дней.");

        //Sorting by chance of be fired
        System.out.println("\nСортировка по вероятности не закончить обучение:");
        students.sort(Comparator.comparingDouble(Calculations::calculateChance));
        students.forEach(Calculations::printChance);

    }
}
