package com.company.app.domain;


public class Course {
    private String courseName;
    private int duration;

    private String getCourseName() {
        return courseName;
    }

    public int getDuration() {
        return duration;
    }

    public Course(String courseName, int duration) {

        this.courseName = courseName;
        this.duration = duration;
    }
    public String toString() {
        return getCourseName() + " - " + getDuration();
    }
}
