package com.company.app.domain;

import java.time.LocalDate;
import java.util.Arrays;

public class Student {
    private String name;
    private Curriculum curriculum;
    private LocalDate start_date;
    private int[] marks;

    public Student(String name, Curriculum curriculum, LocalDate start_date, int[] marks) {

        this.name = name;
        this.curriculum = curriculum;
        this.start_date = start_date;
        this.marks = marks;
    }

    public int[] getMarks() {
        return marks;
    }

    public String getName() {
        return name;
    }

    public Curriculum getCurriculum() {
        return curriculum;
    }

    public LocalDate getStart_date() {
        return start_date;
    }

    public String toString() {
        return "\n" + getName() + "\n" + getCurriculum() + "\n" + getStart_date() + "\n" + Arrays.toString(getMarks());
    }
}
