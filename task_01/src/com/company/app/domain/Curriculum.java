package com.company.app.domain;


import java.util.List;

public class Curriculum {
    private String currName;
    private List<Course> courses;

    private String getCurrName() {
        return currName;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public Curriculum(String currName, List<Course> courses) {

        this.currName = currName;
        this.courses = courses;
    }

    public String toString() {
        return getCurrName() + "\n" + getCourses();
    }
}
