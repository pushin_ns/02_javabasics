package com.company.app.calculation;


import com.company.app.domain.Course;
import com.company.app.domain.Student;

import java.time.LocalDate;
import java.util.List;

public class Calculations {

    //Returns average mark for one student
    public static double getAverage(Student student) {
        double sum = 0;
        int[] marks = student.getMarks();
        int length = marks.length;
        for (int mark : marks) sum += mark;
        return sum / length;
    }

    private static int getFullPeriodInDays(Student student) {
        double fullDuration = 0;
        List<Course> courses = student.getCurriculum().getCourses();
        for (Course course : courses)
            fullDuration += course.getDuration();
        double days = fullDuration / 8;
        return (int) Math.ceil(days);
    }

    public static int calculateDaysUntilEnd(Student student) {
        return getFullPeriodInDays(student) - student.getMarks().length;
    }

    //Calculates the date when training ends
    public static LocalDate calculateEndDate(Student student) {
        return student.getStart_date().plusDays(calculateDaysUntilEnd(student));
    }

    //Calculates chance to finish training successfully.
    // If student have no way to finish the training, he has zero chance. Else - his chance is rising with number of remaining days
    public static double calculateChance(Student student) {
        double chance = calculateDaysUntilEnd(student) * 5 - (getFullPeriodInDays(student) * 4.5 - getSummaryOfMarks(student.getMarks()));
        return (chance < 0) ? 0 : chance;
    }

    public static void calculateProgress(Student student) {
        double avg = getAverage(student);
        System.out.print("Средняя оценка студента " + student.getName() + " - ");
        if (4.5d - avg < 0.0000001)
            System.out.println(avg + " Успеваемость студента на должном уровне.");
        else
            System.out.println(avg + " Студент может быть отчислен.");
    }

    //Prints student with his AVG and days until end
    public static void printAvgAndDays(Student student) {
        System.out.println(student.getName() + ", AVG: " + getAverage(student) + ", Days: " + calculateDaysUntilEnd(student) + ", Chance: " + calculateChance(student));
    }

    public static void printChance(Student student) {
        System.out.println(student.getName() + ": " + calculateChance(student));
    }

    private static int getSummaryOfMarks(int[] arr) {
        int sum = 0;
        for (int a : arr) {
            sum += a;
        }
        return sum;
    }
}
