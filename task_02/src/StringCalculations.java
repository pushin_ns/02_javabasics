import java.util.*;

public class StringCalculations {

    private static final String SOURCE_STRING = "3 * 3 / 2 * 14.2";

    public static void main(String[] args) {
        String[] array = SOURCE_STRING.split("\\s");
        List<String> list = new LinkedList<>(Arrays.asList(array));
        for (int i = 0; i < list.size(); ++i) {
            String s = list.get(i);
            switch (s) {
                case "*": {
                    list.set(i - 1, Double.parseDouble(list.get(i - 1)) * Double.parseDouble(list.get(i + 1)) + "");
                    moveElements(list, i);
                    i--;
                    break;
                }
                case "/": {
                    list.set(i - 1, Double.parseDouble(list.get(i - 1)) / Double.parseDouble(list.get(i + 1)) + "");
                    moveElements(list, i);
                    i--;
                    break;
                }
            }

        }
        for (int i = 0; i < list.size(); ++i) {
            String s = list.get(i);
            switch (s) {
                case "+": {
                    list.set(i - 1, Double.parseDouble(list.get(i - 1)) + Double.parseDouble(list.get(i + 1)) + "");
                    moveElements(list, i);
                    i--;
                    break;
                }
                case "-": {
                    list.set(i - 1, Double.parseDouble(list.get(i - 1)) - Double.parseDouble(list.get(i + 1)) + "");
                    moveElements(list, i);
                    i--;
                }
            }

        }
        System.out.println(list);
    }

    private static void moveElements(List<String> list, int i) {
        list.remove(i + 1);
        list.remove(i);
    }
}
